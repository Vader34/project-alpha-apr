from django.urls import path
from projects.views import show_projects, show_project, create_project

urlpatterns = [
    path("create/", create_project, name="create_project"),
    path("<int:id>/", show_project, name="show_project"),
    path("", show_projects, name="list_projects"),
]
