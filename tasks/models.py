from django.db import models
from django.contrib.auth.models import User
from projects.models import Project

# Create your models here.


class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField(auto_now_add=False, null=False)
    due_date = models.DateTimeField(auto_now_add=False, null=False)
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project, related_name="tasks", on_delete=models.CASCADE
    )
    assignee = models.ForeignKey(
        User,
        null=True,
        blank=True,
        related_name="tasks",
        on_delete=models.CASCADE,
    )
