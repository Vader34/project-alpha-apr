from django.urls import path
from tasks.views import create_task, show_task, edit_task

urlpatterns = [
    path("mine/", show_task, name="show_my_tasks"),
    path("create/", create_task, name="create_task"),
    path("tasks/<int:id>/edit/", edit_task, name="edit_task")
]
